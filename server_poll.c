#include <sys/socket.h>
#include <sys/types.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <netinet/ip.h>
#include <poll.h>
#include <signal.h>

#define MAX_QUEUE 5
#define MEM_INC_SIZE 8
#define BUF_SIZE 256
#define PORTNUM 8080

int Flag=0;

void SigHndlr(int s){
    Flag=1;
}

void freemas(char **name, int size){
    int i;
    for (i=size; i>=0; i--)
        free(name[i]);
}

void nullmas(int *mas, int size){
    int i;
    for (i=0; i<size; i++)
        mas[i]=0;
}

int main(int argc, char *argv[]){
    int main_socket, port, clients, max_clients, events, temp_socket, i, j, cmp=0, cmpnames=0;
    ssize_t n_read;
    char buf[BUF_SIZE], sendbuf[2*BUF_SIZE+2];
    char ** names;
    int *prnames;
    struct sockaddr_in adr;
    struct pollfd * fds, * temp_fds; 

    if (argc<2){
        printf("Необходимо указать номер порта\n");
        return 1;
    }
    port=atoi(argv[1]);

    adr.sin_family = AF_INET;
    adr.sin_port = htons(port);
    adr.sin_addr.s_addr = INADDR_ANY;

    errno = 0;
    signal(SIGINT, SigHndlr);
    main_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (main_socket == -1){
        fprintf(stderr, "%s (%d): Сокет не был создан: %s\n",
                __FILE__, __LINE__ - 2,  strerror(errno));  
        exit(1);
    }

    errno = 0;
    if(bind(main_socket, (struct sockaddr *) &adr, sizeof(adr)) == -1){
        fprintf(stderr, "%s (%d): Не удалось привязать к адресу: %s\n",
                __FILE__, __LINE__ - 2,  strerror(errno));  
        exit(1);
    }
    
    errno = 0;
    if(listen(main_socket, MAX_QUEUE) == -1){
        fprintf(stderr, "%s (%d): Не удалось установить сокет в режим TCP: %s\n",
                __FILE__, __LINE__ - 2,  strerror(errno));  
        exit(1);
    }

    max_clients = MEM_INC_SIZE;
    clients = 0;

    errno =  0;
    fds = malloc(sizeof(struct pollfd) * (max_clients + 1));
    names=malloc(sizeof(char *)*(max_clients));
    prnames=malloc(sizeof(int)*(max_clients));
    nullmas(prnames, max_clients);
    if(fds == NULL){
        fprintf(stderr, "%s (%d): Структура не была создана: %s\n",
                __FILE__, __LINE__ - 2,  strerror(errno));  
        exit(1);
    }

    fds[0].fd = main_socket;
    fds[0].events = POLLIN | POLLERR | POLLPRI | POLLOUT;
    fds[0].revents = 0;


    while(Flag!=1){
        events = poll(fds, clients + 1, 100);
        if(events == -1){
            if (errno!=EINTR){
            fprintf(stderr, "%s (%d): Проблемы с poll: %s\n",
                    __FILE__, __LINE__ - 2,  strerror(errno));  
            exit(1);
            }
        }

        if(events == 0)
            continue;
        
        //printf("Events = %d\n",events);

        if(fds[0].revents){
            temp_socket = accept(main_socket, NULL, NULL);
            if(temp_socket == -1){
                fprintf(stderr, "%s (%d): Не удалось принять: %s\n",
                        __FILE__, __LINE__ - 2,  strerror(errno));  
                exit(1);
            }
            clients++;
            if(clients >= max_clients){
                max_clients += MEM_INC_SIZE;
                temp_fds = fds;
                fds = realloc(fds, sizeof(struct pollfd) * (max_clients + 1));
                names = realloc(names, sizeof(char *)*(max_clients));
                prnames=realloc(prnames, sizeof(int)*(max_clients));
                if(fds == NULL){
                    fprintf(stderr, "%s (%d): Ошибка realloc: %s\n",
                            __FILE__, __LINE__ - 2,  strerror(errno));  
                    free(temp_fds);
                    exit(1);
                }
            }

            fds[clients].fd = temp_socket;
            fds[clients].events = POLLIN | POLLERR | POLLPRI | POLLHUP;
            fds[clients].revents = 0;
            //write(temp_socket,"server-209 v0.0.1\n",strlen("server-209 v0.0.1\n"));
            //shutdown(temp_socket, SHUT_WR);
            fds[0].revents=0;

            read(fds[clients].fd, buf, BUF_SIZE);
            names[clients-1]=(char*)malloc(strlen(buf)+1);
            if (clients>1){
                cmp=0;
                memset(sendbuf, 0, sizeof(sendbuf));
                while (cmp!=1){
                    cmp=0;
                    for (j=1; j<=clients-1; j++){
                        if ((strcmp(buf, names[j-1])==0)&&(fds[j].fd!=-1)){
                            cmp=-1;
                            sprintf(sendbuf, "Такое имя уже существует");
                            send(fds[clients].fd, sendbuf, strlen(sendbuf)+1, 0);
                            free(names[clients-1]);
                        }
                    }
                    if (cmp!=-1){
                        cmp=1;
                        sprintf(sendbuf, "***Клиент %s подсоединился\n", buf);
                        send(fds[clients].fd, sendbuf, strlen(sendbuf)+1, 0);
                    }
                    if (cmp==-1){
                        memset(buf, 0, sizeof(buf));
                        read(fds[clients].fd, buf, BUF_SIZE);
                        names[clients-1]=(char*)malloc(strlen(buf)+1);
                    }
                } 
                for (j=1; j<=clients; j++)
                    send(fds[j-1].fd, sendbuf, strlen(sendbuf)+1, 0);
            }
            else {
                sprintf(sendbuf, "***Клиент %s подсоединился\n", buf);
                send(fds[clients].fd, sendbuf, strlen(sendbuf)+1, 0);
            }
            memset(sendbuf, 0, sizeof(sendbuf));
            strcpy(names[clients-1], buf);
            printf("***Клиент %s подсоединился\n", names[clients-1]);
        }

        for(i = 1; i <= clients; i++){
            if(fds[i].revents){
                memset(buf, 0, sizeof(buf));
                memset(sendbuf, 0, sizeof(sendbuf));
                n_read = read(fds[i].fd, buf, BUF_SIZE);
                if(n_read == 0){
                    printf("***Клиент %s отсоединился\n",names[i-1]);
                    close(fds[i].fd);
                    fds[i].fd = -1;
                }
                if(n_read == -1){
                    fprintf(stderr, "%s (%d): Ошибка при чтении из сокета: %s\n",
                            __FILE__, __LINE__ - 2,  strerror(errno));
                    close(fds[i].fd);
                    fds[i].fd = -1;
                }
                if(n_read > 0){
                    buf[n_read]='\0';
                    if (strncmp(buf, "\\quit", 5) == 0 && (buf[5]==' ' || buf[5]==0)) {
                        sprintf(sendbuf, "***Пользователь %s покидает чат: %s\n", names[i-1], &buf[5]);
                        printf("%s\n", sendbuf);
                        close(fds[i].fd);
                        fds[i].fd = -1;
                    }
                    else if (strcmp(buf, "\\users")==0){
                        for (j=1; j<=clients; j++){
                            if (fds[j].fd!=-1) {
                                printf("%s\n", names[j-1]);
                                sprintf(sendbuf, "%s\n", names[j-1]);
                                send(fds[i].fd, sendbuf, strlen(sendbuf)+1, 0);
                            }
                        }
                        memset(sendbuf, 0, sizeof(sendbuf));
                    }
                    else if (strcmp(buf, "\\privates")==0){
                        for (j=0; j<clients; j++)
                            if (prnames[j]==i){
                                sprintf(sendbuf, "%s\n", names[j]);
                                send(fds[i].fd, sendbuf, strlen(sendbuf)+1, 0);
                                sleep(1);
                            }
                        memset(sendbuf, 0, sizeof(sendbuf));
                    }
                    else if (strncmp(buf, "\\private", 8)==0 && buf[8]==' '){
                        cmpnames=0;
                        for (j=1; j<=clients; j++){
                            if ((strncmp(&buf[9], names[j-1], strlen(names[j-1]))==0)&&(buf[9+strlen(names[j-1])]==' ' || buf[9+strlen(names[j-1])]==0))
                                cmpnames=j;
                        }
                        if (cmpnames==0){
                            sprintf(sendbuf, "Такого пользователя не существует\n");
                            send(fds[i].fd, sendbuf, strlen(sendbuf)+1, 0);
                        }
                        else {
                            sprintf(sendbuf, "%s: *%s\n", names[i-1], &buf[10+strlen(names[cmpnames-1])]);
                            send(fds[cmpnames].fd, sendbuf, strlen(sendbuf)+1, 0);
                            prnames[cmpnames-1]=i;
                        }
                        memset(sendbuf, 0, sizeof(sendbuf));
                    }
                    else if (strcmp(buf, "\\help")==0){
                        sprintf(sendbuf, "Допустимые команды:\n\\users - список пользователей онлайн\n\\private <nickname> <message> - приватное сообщение пользователю <nickname>\n\\privates - список пользователей, которым вы посылали приватное сообщение\n\\quit - выход из чата\n\\help - вывод допустимых команд\n");
                        send(fds[i].fd, sendbuf, strlen(sendbuf)+1, 0);
                        memset(sendbuf, 0 , sizeof(sendbuf));
                    }
                    else {
                        if (buf[0]!=0){
                        printf("%s: %s\n", names[i-1], buf);
                        sprintf(sendbuf, "%s: %s\n", names[i-1], buf);
                        }
                    }
                    for (j=1; j<=clients; j++){
                        send(fds[j].fd, sendbuf, strlen(sendbuf)+1, 0);
                    }
                    memset(buf, 0, sizeof(buf));
                    memset(sendbuf, 0, sizeof(sendbuf));
                }
            }
            fds[i].revents = 0;
        }
    }
    sprintf(sendbuf, "***Сервер завершает работу\n");
    printf("%s", sendbuf);
    for (j=1; j<=clients; j++)
        send(fds[j].fd, sendbuf, strlen(sendbuf)+1, 0);
    free(fds);
    freemas(names, clients-1);
    free(names);
    free(prnames);
    return 0;
}
