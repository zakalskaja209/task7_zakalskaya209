#include <sys/socket.h>
#include <sys/types.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <wait.h>
#include <poll.h>

#define PORTNUM 8080
#define BUFLEN 256


void printmes(char *mes, int size){
    int i=0;
    while ((i<size)&&(mes[i]!='\0')){
        putchar(mes[i]);
        i++;
    }
}

int writemes(char *mes, int size){
    int i=0;
    char c, c1=0;
    while ((c=getchar())==' '){};
    if (c!=' '&& c!='\n' && c!='\r' && c!=EOF){
        mes[i]=c;
        i++;
    }
    while (((c=getchar())!='\n') && (c!='\r') && (c!=EOF)){
        if (!((c1==' ') && (c==' '))){
            mes[i]=c;
            i++;
            c1=c;
        }
    }
    if (i>=size){
        printf("\nСлишком большое сообщение\n");
        memset(mes, 0, size);
        return -1;
    }
    return 1;
}

int main(int argc, char *argv[]){
    int fd = socket(AF_INET, SOCK_STREAM, 0);
    int IP, port, correct_wr;
    struct sockaddr_in adr;
    char buf[BUFLEN], recvbuf[2*BUFLEN+2];
    struct pollfd sock_fd[2];
    if (argc<3){
        printf("Необходимо указать адрес компьютера и номер порта\n");
        return 1;
    }
    IP=atoi(argv[1]);
    port=atoi(argv[2]);
    memset(&adr, 0, sizeof(adr));
    setbuf(stdout, NULL);
    adr.sin_family = AF_INET;
    adr.sin_port = htons(port);
    adr.sin_addr.s_addr=IP;
    if (inet_pton(AF_INET, argv[1], &adr.sin_addr) == -1){
        fprintf(stderr, "%s (%d): Ошибка подключения: %s\n",
                __FILE__, __LINE__ - 2,  strerror(errno));  
        exit(1);
    } 
    if (connect (fd, (struct sockaddr *) &adr, sizeof adr) == -1){
        fprintf(stderr, "%s (%d): Не удалось установить соединение с сервером: %s\n",
                __FILE__, __LINE__ - 2,  strerror(errno));  
        exit(1);
    }
    sock_fd[0].fd=0;
        sock_fd[0].events=POLLIN | POLLERR | POLLPRI/* | POLLOUT*/;
        sock_fd[0].revents=0;
	sock_fd[1].fd=fd;
	sock_fd[1].events=POLLIN | POLLERR | POLLPRI/* | POLLOUT*/;
	sock_fd[1].revents=0;
    
    printf("Имя пользователя: ");
    scanf("%s", buf);
    write(fd, buf, strlen(buf)+1);
    memset(buf, 0, BUFLEN);
    read(sock_fd[1].fd, recvbuf, sizeof(recvbuf));
    while (strcmp(recvbuf, "Такое имя уже существует")==0){
        printmes(recvbuf, strlen(recvbuf));
        putchar('\n');
        printf("Имя пользователя: ");
        scanf("%s", buf);
        write(fd, buf, strlen(buf)+1);
        memset(buf, 0, sizeof(buf)+1);
        read(sock_fd[1].fd, recvbuf, sizeof(recvbuf));
        memset(recvbuf, 0, sizeof(recvbuf));
    }
    
    while (1){
        if (fork()){
            correct_wr=writemes(buf, BUFLEN);
            if (correct_wr==1){
                if (strncmp(buf, "\\quit", 5) == 0 && (buf[5]==' ' || buf[5]==0)) {
                    send(sock_fd[1].fd, &buf, BUFLEN, 0);
                    return 0;
                }
                else {
                    send(sock_fd[1].fd, &buf, BUFLEN, 0);
                }
            }
            memset(buf, 0, sizeof(buf));
        }
        else {
            while (read(sock_fd[1].fd, recvbuf, sizeof(recvbuf))>0){
                printmes(recvbuf, sizeof(recvbuf));
                //printf("%s\n", recvbuf);
                memset(recvbuf, 0, sizeof(recvbuf));
            }
            return 0;
        }
    }
    close(sock_fd[1].fd);
    return 0;
}
